# Report: Predict Bike Sharing Demand with AutoGluon Solution
#### NAME HERE

## Initial Training
### What did you realize when you tried to submit your predictions? What changes were needed to the output of the predictor to submit your results?
TODO: I didnot need ressting values less tahn 0 to 0.
however, I had initially split the datetime column into indivisual components even in the submission file which I had to go back and correct.

### What was the top ranked model that performed?
TODO: WeightedEnsemble_L3  

## Exploratory data analysis and feature creation
### What did the exploratory analysis find and how did you add additional features?
TODO: the datetime column was an object. set that to datetime and split into year, month, day, hour.
Min and second were always 0 so didnt matter.
categiry variables were set as category type for better identification by models

### How much better did your model preform after adding additional features and why do you think that is?
TODO: The model performed worse. that is because it was able to make better sense of time of day which was critical for the ride sharing problem.

## Hyper parameter tuning
### How much better did your model preform after trying different hyper parameters?
TODO: the model performance improved a bit.

### If you were given more time with this dataset, where do you think you would spend more time?
TODO: i wouldmake better features. one hot encoding.

### Create a table with the models you ran, the hyperparameters modified, and the kaggle score.
model	hpo1	hpo2	hpo3	score
0	initial	prescribed_values	prescribed_values	presets: 'high quality' (auto_stack=True)	1.84484
1	add_features	prescribed_values	prescribed_values	presets: 'high quality' (auto_stack=True)	0.44798
2	hpo (top-hpo-model: hpo2)	Tree-Based Models: (GBM, XT, XGB & RF)	KNN	presets: 'optimize_for_deployment	0.49440

### Create a line plot showing the top model score for the three (or more) training runs during the project.

TODO: Replace the image below with your own.

![model_train_score.png](img/model_train_score.png)

### Create a line plot showing the top kaggle score for the three (or more) prediction submissions during the project.

TODO: Replace the image below with your own.

![model_test_score.png](img/model_test_score.png)

## Summary
In summary, giving the right information to the modelin form of right features makes formore optimum predictions.hyper parameters can further make it more accurate
