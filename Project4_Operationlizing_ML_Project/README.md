# Operationalizing-an-AWS-ML-Project

**Project developed for AWS Machine Learning Engineer Scholarship offered by Udacity (2024)**


The right configuration for deployment is a very important step in machine learning operations as its can avoid problems such as high costs and bad performance. Some examples of configurations for production deployment of a model includes computer resources such as machine instance type and number of instances for training and deployment, security since poor security configuration can leads to data leaks or performance issues. By implement the right configuration we can have a high-throughtput and low-lantecy machine learning model in production.

---------

## Setup notebook instance

Finding SageMaker in AWS
In SageMaker we then create a notebook instance in Notebook -> Notebook Instances -> Create notebook instance button
Then we create a new instance choosing a notebook instance name and type. For this project ml.m5.xlarge instance type was selected
With the notebook instance create we can upload a jupyter notebook, the script.py file to train our deep learning image classification model, the inference.py script and an image for test. Bellow you can find the list of files that we need to upload:
train_and_deploy-solution.ipynb
hpo.py
inference2.py
lab.jpg
Before we can start to train our model we need first create a S3 bucket where we will upload our train, validation and test data to. So let's do it now :)


---------

## Setup S3 

Finding s3 

Next, we create a new bucket by clicking in create a new bucket button and give our S3 bucket a unique name


---------

## Hyperparameter tunning

### Defining enviroment variables for hyperparameter tunning

SM_CHANNEL_TRAINING: where the data used to train model is located in AWS S3

SM_MODEL_DIR: where model artifact will be saved in S3

SM_OUTPUT_DATA_DIR: where output will be saved in S3


Here we are passing some paths to our S3 which will be used by the notebook instance to get data, save model and output
```python
os.environ['SM_CHANNEL_TRAINING']='s3://mysolnbucket/'
os.environ['SM_MODEL_DIR']='s3://mysolnbucket/model/'
os.environ['SM_OUTPUT_DATA_DIR']='s3://mysolnbucket/output/'
```

Here we can see how we can access the enviroment variables in hpo.py script

```python
if __name__=='__main__':
    parser=argparse.ArgumentParser()
    parser.add_argument('--learning_rate', type=float)
    parser.add_argument('--batch_size', type=int)
    parser.add_argument('--data', type=str, default=os.environ['SM_CHANNEL_TRAINING'])
    parser.add_argument('--model_dir', type=str, default=os.environ['SM_MODEL_DIR'])
    parser.add_argument('--output_dir', type=str, default=os.environ['SM_OUTPUT_DATA_DIR'])
    
    args=parser.parse_args()
```

For this model two hyperparameters was tunning: learning rate and batch size.

```python
hyperparameter_ranges = {
    "learning_rate": ContinuousParameter(0.001, 0.1),
    "batch_size": CategoricalParameter([32, 64, 128, 256, 512]),
}
```

Bellow you can see how hyperparameter tuner and estimator was defined. Notice that we are using a py script (hpo.py) as entry point to the estimator, this script contains the code need to train model with different hyperparameters values.

```python
estimator = PyTorch(
    entry_point="hpo.py",
    base_job_name='pytorch_dog_hpo',
    role=role,
    framework_version="1.4.0",
    instance_count=1,
    instance_type="ml.g4dn.xlarge",
    py_version='py3'
)

tuner = HyperparameterTuner(
    estimator,
    objective_metric_name,
    hyperparameter_ranges,
    metric_definitions,
    max_jobs=2,
    max_parallel_jobs=1,  # you once have one ml.g4dn.xlarge instance available
    objective_type=objective_type
)

tuner.fit({"training": "s3://mysolnbucket/data/"})
```

> **Note**
> We are passing a S3 path where the data for training, validation and testing are loacated to the HyperparameterTuner fit method
  

After we start the model training we can see the training job status at SageMaker -> Training -> Training Jobs

## Training Model with best hyperparameters values

**Without multi-instance**

Notice that training a model without enable multi-instance took 21 minutes to complete


**Deploying model**

We can check the deployed model in SageMaker -> Inference -> Endpoints

Notice that the model was deployed with one initial instance and a instance type which uses the type ml.m5.large 

```python
predictor = pytorch_model.deploy(initial_instance_count=1, instance_type='ml.m5.large')
```

---------

## EC2 Setup

EC2 as others AWS services can be founded by search it by name in AWS 

Now we can create our new instance by clicking in Launch instances button

First, we must give a name to our instance 

We are now selecting an Amazon Machine Image (AMI), which is a supported and maintained image provided by AWS that contains the necessary information to launch an instance. Since we will be training a deep learning model with PyTorch, we need to select an AMI that supports PyTorch for deep learning.

Next, we need to choose an EC2 instance that is supported by this AMI. According to the documentation, this type of AMI supports the following instance:  Inf1, Inf2, Trn1, Trn1n
First I compare all available models under INF1 (INF1.xlarge, INF1.2xlarge, INF1.6xlarge, INF1.24xlarge)
INF2 - INF2.xlarge, INF2.8xlarge, INF2.24xlarge, INF2.48xlarge
TRN1 - TRN1.2xlarge, trn132xlarge
TRN1N - TRN1n.32xlarge
INF2 has Inferentia2 accelerators which makes it much more costlier than INF1.
Similarly TRN1 has Trainium Accelerators which increases its cost.
I didnot need these accelerators and thesefor I go with INF1.
Furthermore, when I look within INF1 options, I have only one option with 4 vCPU.
I chose INF1.Xlarge which has 4 vCPUs and 8 GiB memory.
it costs $0.228/Hr.


> **Note**
> To simplify things, other configurations will be set to their default values


### EC2 vs Notebook instance for training models

Both services have their own advantages:

EC2 instances can be easily scaled up or down based on computing needs, can be customized to meet specific requirements such as framework (pytroch or tensorflow), number of CPUs, memory size and GPU support and EC2 instances can be optimized for high-performance computing, which can greatly reduce the time it takes to train large machine learning models.

Notebook instances have their own advantages too such as: quick setup as they comes with pre-configured with popular machine learning frameworks and libraries, easy collaboration and integration with others AWS services such as AWS SageMaker, which provides a lot of tools required for machine learning engineering and operations.

------------

## Lambda Functions Setup


Adding SageMaker access permission to Lambda Function
SageMakerFullAccess policy was added to Lambda to ensure it can call the endpoint created using sagemaker.
This was the only policy applied to maintain least priviledge access.

## Scaling Lambda Function and Endpoint

### Adding concurrency to Lambda Function

By default a Lambda Function can only respond one request at once. One way to change that is to use concurrency so that the Lambda Function can be responds to multiple requests at once. Before add concurrency we need to configure a version which can be done in Configuration tab.


Add a description for the new version and click on Publish button

Now we can add concurrency in Configuration -> Provisioned concurrency -> Edit button

Our final task is to select an integer value for concurrency. Provisioned concurrency initializes a specified number of execution environments, enabling them to respond immediately. Therefore, a higher concurrency level results in reduced latency. In this example the concurrency was set to two so this Lambda Functions can handle two requests at once which could not be enough for services with high demand.

---------
### Auto-Scaling endpoint

Auto scaling is a powerful feature of SageMaker that allows for dynamic adjustment of the number of instances used with deployed models based on changes in workload. With auto scaling, SageMaker automatically increases or decreases the number of instances, ensuring that we only pay for the instances that are actively running.

We can enable auto-scaling in SageMaker -> Endpoints -> Endpoint runtime settings

---------

